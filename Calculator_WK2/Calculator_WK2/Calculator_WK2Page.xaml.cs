﻿using System;
using Xamarin.Forms;

namespace Calculator_WK2
{
    public partial class Calculator_WK2Page : ContentPage
    {
        //result holds the result of calculations
        //input1 is the first input the user enters, before algerbraic expression
        //input2 is the second input the user enters, befer equals sign
        //count makes sure the default 0 is not included in output string as users enters values
        //exprCount allows for user to only use 1 expression in input before pressing equals
        //expr is the value related to the desired expression user enters
        //phase allows for program to know if value entered was before or after equals sign determing which input value goes into
        double result,input1, input2, count , exprCount, expr,phase = 0;

        public Calculator_WK2Page()
        {
            InitializeComponent();
        }
        // start of handling number inputs-----------------------------
        void Handle_Clicked0(object sender, System.EventArgs e)
        {
            if (count == 0)
            {
                Output.Text = zero.Text;
                count++;
            }
            else
            {
                Output.Text += zero.Text;
            }   // handle 0 
        }
        void Handle_Clicked1(object sender, System.EventArgs e)
        {
            if (count == 0)
            {
                Output.Text = one.Text;
                count++;
            }
            else
            {
                Output.Text += one.Text;
            }// handle 1
        }

        void Handle_Clicked2(object sender, System.EventArgs e)
        {
            if (count == 0)
            {
                Output.Text = two.Text;
                count++;
            }
            else
            {
                Output.Text += two.Text;
            }// handle 2
        }

        void Handle_Clicked3(object sender, System.EventArgs e)
        {
            if (count == 0)
            {
                Output.Text = three.Text;
                count++;
            }
            else
            {
                Output.Text += three.Text;
            }// handle 3
        }
        void Handle_Clicked4(object sender, System.EventArgs e)
        {
            if (count == 0)
            {
                Output.Text = four.Text;
                count++;
            }
            else
            {
                Output.Text += four.Text;
            }// handle 4
        }

        void Handle_Clicked5(object sender, System.EventArgs e)
        {
            if (count == 0)
            {
                Output.Text = five.Text;
                count++;
            }
            else
            {
                Output.Text += five.Text;
            }// handle 5 
        }

        void Handle_Clicked6(object sender, System.EventArgs e)
        {
            if (count == 0)
            {
                Output.Text = six.Text;
                count++;
            }
            else
            {
                Output.Text += six.Text;
            }// handle 6
        }
        void Handle_Clicked7(object sender, System.EventArgs e)
        {
            if (count == 0)
            {
                Output.Text = seven.Text;
                count++;
            }
            else
            {
                Output.Text += seven.Text;
            }// handle 7
        }

        void Handle_Clicked8(object sender, System.EventArgs e)
        {
            if (count == 0)
            {
                Output.Text = eight.Text;
                count++;
            }
            else
            {
                Output.Text += eight.Text;
            }// handle 8
        }

        void Handle_Clicked9(object sender, System.EventArgs e)
        {
            if (count == 0)
            {
                Output.Text = nine.Text;
                count++;
            }
            else
            {
                Output.Text += nine.Text;
            }// handle 9
        }
        // end of handling number inputs---------------------------------

        void Handle_ClickedReset(object sender, System.EventArgs e)
        {
            Output.Text = "0";
            count = 0;// handle reset 'C'
        }

        void Handle_ClickedDivide(object sender, System.EventArgs e)
        {
            if (count == 0)
            {
            }
            else
            {
                double.TryParse(Output.Text, out input1);
                Output.Text = "0";
                exprCount++;
                phase++;
                count = 0;
                expr = 1;
            }// handle divide
        }

        void Handle_ClickedMultiply(object sender, System.EventArgs e)
        {
            if (count == 0)
            {
            }
            else
            {
                double.TryParse(Output.Text, out input1);
                Output.Text = "0";
                exprCount++;
                phase++;
                count = 0;
                expr = 2;
            }// handle multiply
        }

        void Handle_ClickedSubtract(object sender, System.EventArgs e)
        {
            if (count == 0)
            {
            }
            else
            {
                double.TryParse(Output.Text, out input1);
                Output.Text = "0";
                exprCount++;
                phase++;
                count = 0;
                expr = 3;
            }// handle subtract
        }

        void Handle_ClickedPlus(object sender, System.EventArgs e)
        {
            if (count == 0)
            {
            }
            else
            {
                double.TryParse(Output.Text, out input1);
                Output.Text = "0";
                exprCount++;
                phase++;
                count = 0;
                expr = 4;
            }// handle plus
        }

        void Handle_ClickedEquals(object sender, System.EventArgs e)
        {
            // handle equals
            if (exprCount > 1)
            {
                Output.Text = "0";
                Reset();
            }
            else if(phase != 1)
            {
                Output.Text = "0";
                Reset();
            }
            else
            {
                double.TryParse(Output.Text, out input2);

                if (expr == 1) // handle divide
                {
                    if (input2 == 0)
                    {
                        result = 0;
                    }
                    else
                    {
                        result = input1 / input2;
                    }
                }
                else if (expr == 2) // handle multiply
                {
                    result = input1 * input2;
                }
                else if (expr == 3) // handle subtraction
                {
                    result = input1 - input2;
                }
                else if (expr == 4) // handle addition
                {
                    result = input1 + input2;
                }
                Output.Text = result.ToString();
                Reset();
            }
        }
        void Reset()
        // results all counters so calcualtor can restart
        {

            result = input1 = input2 = count = exprCount = expr = phase = 0;
        }
    }
}
